#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
La clase shelf consiste en donde estarán todo los libros, se usará
para explorar que hay en la biblioteca sin la necesidad de tomar el libro
Ademas Shelf será usada para obtener el libro que se desea
Ademas la información es segura, debido a que se usa las variables en privado
Para obtener una variable, se hara un get"X", donde X es el nombre
Para actualizar una variable, se hara un set"X", donde X es el nombre
Se pueden ver todo los libros con informacion completa, gracias a __str__
"""


class Shelf():
    def __init__(self, Name, Year, Author, Code, Disp):
        """
        Se definirá que libro se estan visualizando
        na = Nombre
        ye = Año de lanzamiento
        Aut = Autor
        Code = Codigo
        Disp = Disponibilidad
        """
        self.__na = Name
        self.__ye = Year
        self.__Aut = Author
        self.__Code = Code
        self.__Disp = Disp

    def __str__(self):
        lib = self.na+", "+self.ye+", "+self.Aut+", "+self.Code+", "+self.Disp
        return lib

    def getname(self):
        return self.na

    def getyear(self):
        return self.ye

    def getauthor(self):
        return self.Aut

    def getcode(self):
        return self.Code

    def getdisponibilidad(self):
        return self.Disp

    def setname(self, New):
        self.na = New

    def setyear(self, New):
        self.ye = New

    def setauthor(self, New):
        self.Aut = New

    def setcode(self, New):
        self.Code = New

    def setdisponibilidad(self, New):
        self.Disp = New
