#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import random
from Libro import Book
from Libreria import Shelf
from os import system, name


def clear():
    if name == 'nt':
        _ = system('cls')
    else:
        _ = system('clear')


def VerLibro(library):
    """
    La funcion de esto es ver los libros que estan disponibles
    Aunque es limitado ya que solo se puede ver el nombre
    """
    x = 0
    while x != 5:
        print("Presione A para ver los libros con toda la informacion")
        print("Presione F para ver los libros con nombre y año de lanzamiento")
        print("Presione C para ver los libros con nombre y codigo")
        opt = input("Ingrese su opcion: \n")
        if opt.upper() == "A":
            x = 5
        elif opt.upper() == "F":
            x = 5
        elif opt.upper() == "C":
            x = 5
        else:
            print("Opcion ingresada no es valida, intente de nuevo")
    print("Los libros en la biblioteca son los siguientes: \n")
    largo = len(library)
    shelf = Shelf(None, None, None, None, None)
    for x in range(largo):
        if x % 5 == 0:
            shelf.setname(library[x])
            shelf.setyear(library[x+1])
            shelf.setauthor(library[x+2])
            shelf.setcode(library[x+3])
            shelf.setdisponibilidad(library[x+4])
            if opt.upper() == "A":
                print(shelf)
                print("\n")
            elif opt.upper() == "F":
                print(shelf.getname())
                print(shelf.getyear())
                print("\n")
            elif opt.upper() == "C":
                print(shelf.getname())
                print(shelf.getcode())
                print("\n")


def CrearLibro(library):
    """
    El usuario creará un libro, en el cual ingresará el nombre, autor y el
    año cuando se creo el libro, lo que viene siendo codigo será creado
    por un valor rand, ademas el estado estará predeterminadamente como
    si estuviera disponible
    El codigo se hará un string, ya que todo el array lo es
    """
    nombre = input("Ingrese el nombre del libro o revista: \n")
    autor = input("Ingrese el autor: \n")
    anio = str(input("Ingrese el año: \n"))
    print("Ingrese el codigo")
    codigo = input("en el caso de estar vacio, será random: \n")
    if codigo == "":
        codigo = random.randint(0, 999999999999)
        codigo = str(codigo)
    estado = "Disponible"
    library.append(nombre)
    library.append(anio)
    library.append(autor)
    library.append(codigo)
    library.append(estado)
    clear()
    print("El libro fue creado y enviado a la libreria \n")


def Obtener(library, Hand):
    """
    Viene a ser la accion se agarrar un libro
    La funcion de esto es que simula que tienes una mano, en esa mano
    tendras el libro y podrás ver cual libro agarraste
    """
    largo = len(library)
    x = 0
    shelf = Shelf(None, None, None, None, None)
    while x != 10:
        VerLibro(library)
        num = int(input("\nsiendo 0 el primer libro, ¿cual desea Obtener?:\n"))
        num = num * 5
        if num < largo:
            """
            Se define cual es el libro que agarro, el cual no será modificado
            a excepcion que el usuario lo haga intencionalmente
            Primero se colocará en el shelf, luego de esto, será transferido
            hacia la mano o Book
            """
            shelf.setname(library[num])
            shelf.setyear(library[num+1])
            shelf.setauthor(library[num+2])
            shelf.setcode(library[num+3])
            shelf.setdisponibilidad(library[num+4])
            Hand.AgarrarLibro(shelf, shelf, shelf, shelf, shelf)
            clear()
            print("El libro obtenido es: ")
            print(Hand)
            return Hand
        else:
            clear()
            print("Numero ingresado no es correcto")
        print("\n")


def Modificar(x, Hand):
    """
    Se modificara si un libro fue devuelto o prestado
    Lo cual será actualizado en el siguiente paso
    """
    print("\n")
    while x != 5:
        print("Si el libro fue prestado, presione G")
        print("Si el libro fue devuelto, presione R")
        print("Si termino de modificar el libro, presione X")
        opt = input("Ingrese su accion: \n")
        clear()
        if opt.upper() == "G":
            Hand.setDisponibilidad("Prestado")
        elif opt.upper() == "R":
            Hand.setDisponibilidad("Disponible")
        elif opt.upper() == "X":
            x = 5
        else:
            print("El input es incorrecto, intente de nuevo")
        print("\n")
        print(Hand)
    return Hand


def Cambiar(Hand, library):
    """
    Se realiza el cambio, en donde se actualiza en la matriz
    asi si se obtiene de nuevo, el cambio existira y no solo estará en la mano
    """
    for x in range(len(library)):
        if library[x] == Hand.getName():
            library[x+4] = Hand.getDisponibilidad()
            print("El cambio fue realizado")
    print("\n")


def Investigar(Hand, x):
    """
    Se investigará el libro de forma individual
    El usuario podrá ver lo que desee al ingeras la opcion
    """
    print("\n")
    while x != 5:
        print("Presione N para vel el nombre")
        print("Presione F para ver el año de publicacion")
        print("Presione A para ver el autor")
        print("Presione C para ver el codigo")
        print("Presione D para ver la disponibilidad")
        print("Presione O para verlo por completo")
        print("Presione X para dejar de investigar")
        Accion = input("Ingrese su accion: \n")
        clear()
        if Accion.upper() == "N":
            print("El nombre del libro es: ")
            print(str(Hand.getName()))
        elif Accion.upper() == "F":
            print("El año que fue publicado el libro es el: ")
            print(str(Hand.getYear()))
        elif Accion.upper() == "A":
            print("El autor del libro es: ")
            print(str(Hand.getAuthor()))
        elif Accion.upper() == "C":
            print("El codigo que tiene el libro es el: ")
            print(str(Hand.getCode()))
        elif Accion.upper() == "D":
            print("La disponibilidad del libro es: ")
            print(str(Hand.getDisponibilidad()))
        elif Accion.upper() == "O":
            print("La información del libro es la siguiente: ")
            print(Hand)
        elif Accion.upper() == "X":
            x = 5
        else:
            print("El input es incorrecto, intente de nuevo")
        print("\n")


def main(library):
    """
    Aca se encuentra el menu, donde el usuario ingresará su accion
    Es imposible completar el while, por lo tanto, es un bucle infinito
    La unica forma de salir, es ingresando x, lo cual termina el programa
    El programa intenta simular que tienes un libro en la mano, y ese
    libro se puede modificar, pero solo la disponibilidad, se pensaba en
    solo obtener un libro por la opcion G, pero en el caso de modificar
    el libro tendrá que ser agarrado de nuevo
    """
    x = 0
    Hand = Book(None, None, None, None, None)
    while x != 10:
        print("Bienvenido al menu")
        print("Presione C para crear un libro")
        print("Presione S para ver los libros que hay")
        print("Presione G para obtener un libro")
        print("Presione M para modificar si un libro fue prestado o devuelto")
        print("Presione I para investigar el libro que tiene agarrado")
        print("Presione X para salir")
        opt = input("Ingrese su acción: \n")
        clear()
        if opt.upper() == "C":
            CrearLibro(library)
        elif opt.upper() == "S":
            VerLibro(library)
        elif opt.upper() == "G":
            Hand = Obtener(library, Hand)
        elif opt.upper() == "M":
            Hand = Obtener(library, Hand)
            Hand = Modificar(x, Hand)
            Cambiar(Hand, library)
        elif opt.upper() == "I":
            if Hand.getName is not None:
                Investigar(Hand, x)
            else:
                clear()
                print("No tiene un libro en mano")
        elif opt.upper() == "X":
            print("\n")
            print("Gracias, vuelva pronto con nuestra gran variedad de libros")
            quit()
        else:
            clear()
            print("boton incorrecto, intente de nuevo")
        print("\n")


if __name__ == '__main__':
    library = ["Las aventuras de Juan", "2000", "Juanito", "291462812538",
               "Prestado"]
    main(library)
