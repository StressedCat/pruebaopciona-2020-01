#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from Libreria import Shelf
"""
Aca es donde la información del libro se guarda, ademas de ser el libro
que será modificado a lo largo de la modificación, se puede decir que es el
libro que se tiene en la mano
Ademas la información es segura, debido a que se usa las variables en privado
Para obtener una variable, se hara un get"X", donde X es el nombre
Para actualizar una variable, se hara un set"X", donde X es el nombre
En el caso que se tenga que ver todo el libro, existe es __str__
"""


class Book():
    def __init__(self, N, Y, A, C, D):
        """
        Se definirá el libro aca
        N = Nombre
        Y = Año de lanzamiento
        A = Autor
        C = Codigo
        D = Disponibilidad
        """
        self.__N = N
        self.__Y = Y
        self.__A = A
        self.__C = C
        self.__D = D

    def __str__(self):
        lib = self.N+", "+self.Y+", "+self.A+", "+self.C+", "+self.D
        return lib

    def getName(self):
        return self.N

    def getAuthor(self):
        return self.A

    def getYear(self):
        return self.Y

    def getCode(self):
        return self.C

    def getDisponibilidad(self):
        return self.D

    def setName(self, New):
        self.N = New

    def setAuthor(self, New):
        self.A = New

    def setYear(self, New):
        self.Y = New

    def setCode(self, New):
        self.C = New

    def setDisponibilidad(self, New):
        self.D = New

    def AgarrarLibro(self, Nombre, Anio, Autor, Codigo, Disponibilidad):
        self.N = Nombre.na
        self.Y = Anio.ye
        self.A = Autor.Aut
        self.C = Codigo.Code
        self.D = Disponibilidad.Disp
        pass
